# About the project #

This project was built as a part of a test case to track the status of specific ports and give JSON based respond,
currently it requires a manual change based on the port you need/want to track.
The project itself was built for Java 1.7 and Tomcat 6.x but can easily be ported.


## Project's structure ##

**PortChecker** - Console based package(Compile to jar)
**WebPortChecker** - Web based package(Compile to war)

## Running and Compling ##

### Console ###
java -jar portChecker.jar

### Web ###
load webPortChecker.war to tomcat, open your favorite browser and check http://localhost:8080/WebPortChecker/