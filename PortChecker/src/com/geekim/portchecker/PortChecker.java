package com.geekim.portchecker;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Port checking class.
 * Created in 2014/08/07
 * @author Eric
 *
 */
public class PortChecker
{
	/**
	 * Constants.
	 */
	private static final String	TOMCAT_KEY		= "tomcat";
	private static final String	POSTGRESQL_KEY	= "postgresql";
	private static final String	CUSTOM_KEY1		= "txtorENcore";
	private static final String	CUSTOM_KEY2		= "PreProcessor";
	
	private static final int	TOMCAT_PORT		= 8080;
	private static final int	POSTGRESQL_PORT	= 5432;
	private static final int	CUSTOM_PORT1	= 50200;
	private static final int	CUSTOM_PORT2	= 60000;

	/**
	 * Default constructor.
	 */
	public PortChecker()
	{
	
	}
	
	/**
	 * Check to see if the service port is listening.
	 * @param port
	 * @return
	 */
	public static boolean isListening(int port)
	{
		boolean	result	= false;
		
		try
		{
			Socket	socket	= new Socket();
			socket.connect(new InetSocketAddress("localhost", port), 1000);
			
			result	= socket.isConnected();
			
			socket.close();
		}
		catch (IOException ioe)
		{
			// ioe.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Get the status of service ports as JSON string.
	 * @return JSON type string.
	 */
	public static String getServicePortsStatus()
	{
		JSONObject	result	= new JSONObject();
		try
		{
			result.put(TOMCAT_KEY, isListening(TOMCAT_PORT) ? 1 : 0);
			result.put(POSTGRESQL_KEY, isListening(POSTGRESQL_PORT) ? 1 : 0);
			result.put(CUSTOM_KEY1, isListening(CUSTOM_PORT1) ? 1 : 0);
			result.put(CUSTOM_KEY2, isListening(CUSTOM_PORT2) ? 1 : 0);
		}
		catch (JSONException jsone)
		{
			jsone.printStackTrace();
		}
		
		return result.toString();
	}

}
